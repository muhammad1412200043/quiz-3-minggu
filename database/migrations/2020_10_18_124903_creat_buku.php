<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatBuku extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('profiles', function (Blueprint $table) {
                $table->bigIncrements('user_id');
                $table->string("judul");
                $table->integer("jumlah_halaman");
                $table->string("tahun_terbit");
                $table->string("genre");
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
